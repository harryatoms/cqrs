from cqrs.commands import Command
from cqrs.aggregates import Aggregate
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, DateTime
from sqlalchemy.dialects.postgresql import JSON
ProjectionBase = declarative_base()


class Projection:
    """
        Base Projection
    """
    @staticmethod
    def project(obj):
        raise NotImplementedError("Projection must implement project")


class CommandProjection(ProjectionBase, Projection):
    """
        Command Projection
    """
    __tablename__ = 'processed_commands'

    id = Column(Integer, primary_key=True)
    command_name = Column(String(250), nullable=False)
    aggregate_type = Column(String(50), nullable=False)
    aggregate_id = Column(String(50), nullable=False)
    created_at = Column(DateTime, nullable=False)
    completed_at = Column(DateTime, nullable=False)
    command_status = Column(String(250), nullable=False)
    error_message = Column(String(500), nullable=True)
    payload = Column(JSON, nullable=True)

    @staticmethod
    def project(cmd: Command):
        return CommandProjection(
            aggregate_type=cmd.aggregate_type.__name__,
            aggregate_id=str(cmd.aggregate_id),
            command_name=cmd.__class__.__name__,
            command_status=cmd.tracking.status,
            created_at=cmd.tracking.created_at,
            completed_at=cmd.tracking.completed_at,
            error_message=cmd.tracking.message,
            payload=cmd.payload
        )


class Projector:

    def __init__(self, session, p_map):
        self.session = session
        self.p_map = p_map

    def project(self, cmd: Command, aggregate: Aggregate = None):
        # persist command projection
        self.session.add(CommandProjection.project(cmd))

        # persist aggregate projections
        if aggregate:
            cls = aggregate.__class__.__name__
            if cls not in self.p_map.keys():
                return

            for projection in [pc.project(aggregate) for pc in self.p_map[cls]]:
                self.session.add(projection)

        # commit transaction
        self.session.commit()
