from overloading import *
import uuid


class AggregateID:
    """
        UUID
    """
    def __init__(self, str_id: str):
        self.id = str_id

    def __str__(self):
        return self.id

    @staticmethod
    def create():
        """
        Generate new UUID

        :return: AggregateId
        """
        return AggregateID(uuid.uuid4().hex)


class Aggregate:
    """
        Command Processing Domain Aggregate
    """
    def __init__(self, aggregate_id: AggregateID):
        self.id = aggregate_id

    @overloaded
    def handle_cmd(self, cmd):
        """
            Process Command
        :param cmd:
        """
        raise NotImplementedError("Command not recognized.")
