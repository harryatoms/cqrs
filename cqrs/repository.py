from cqrs.aggregates import AggregateID


class AggregateRepository:
    """
        Aggregate Repository
    """
    def find(self, aggregate_type, aggregate_id: AggregateID):
        """
            Load Aggregate of Type with id

        :param aggregate_type
        :param aggregate_id: AggregateId
        :return: aggregate_class
        """
        return aggregate_type(aggregate_id)