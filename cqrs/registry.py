

class Registry:
    """
        Command/Query Registry
    """
    commands = dict()
    queries = dict()

    @staticmethod
    def register_command(cmd: type):
        if cmd.__name__ == 'Command':
            return

        Registry.commands[cmd.__name__] = cmd

    @staticmethod
    def register_query(query):
        if query.__name__ == 'Query':
            return

        Registry.commands[query.__name__] = query


class CMD(type):
    """
        Command Metaclass: Used for auto-registry
    """
    def __new__(cls, clsname, bases, attrs):
        cmd_class = super(CMD, cls).__new__(cls, clsname, bases, attrs)
        Registry.register_command(cmd_class)
        return cmd_class


class QUERY(type):
    """
        Query Metaclass: Used for auto-registry
    """
    def __new__(cls, clsname, bases, attrs):
        query_class = super(QUERY, cls).__new__(cls, clsname, bases, attrs)
        Registry.register_query(query_class)
        return query_class
