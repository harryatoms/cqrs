from cqrs.aggregates import AggregateID
from cqrs.registry import CMD, Registry
from datetime import datetime


class Command(metaclass=CMD):
    """
        Command Message
    """
    AGGREGATE_TYPE = None

    @property
    def aggregate_type(self):
        if not self.AGGREGATE_TYPE:
            raise ValueError("Command missing aggregate type")
        return self.AGGREGATE_TYPE

    def __init__(self, aggregate_id: AggregateID, payload: dict = None):
        self.aggregate_id = aggregate_id
        self.payload = payload if payload else dict()
        self.tracking = TrackingInformation()


class TrackingInformation:
    """
        Command Tracking Info
    """
    class CommandStatus:
        CREATED = 'created'
        COMPLETED = 'completed'
        REJECTED = 'rejected'
        FAILED = 'failed'

    def __init__(self):
        self.status = self.CommandStatus.CREATED
        self.created_at = datetime.now()
        self.completed_at = None
        self.message = None

    def finished(self):
        self.status = self.CommandStatus.COMPLETED
        self.completed_at = datetime.now()

    def rejected(self, msg):
        self.status = self.CommandStatus.REJECTED
        self.completed_at = datetime.now()
        self.message = msg

    def failed(self, msg):
        self.status = self.CommandStatus.FAILED
        self.completed_at = datetime.now()
        self.message = msg


class CommandFactory:
    """
        Command Factory
    """
    @staticmethod
    def make_cmd(cmd_name, data=None):
        if cmd_name not in Registry.commands.keys():
            raise ValueError("Command type unknown. Got: {cmd}".format(cmd=cmd_name))

        cmd_class = Registry.commands.get(cmd_name)
        return cmd_class(AggregateID.create(), data)
