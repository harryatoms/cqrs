from cqrs.commands import Command
from cqrs.projections import Projector
from cqrs.repository import AggregateRepository


class CommandProcessor:
    """
        Command Processor
    """
    def __init__(self, repo: AggregateRepository, projector: Projector):
        self.repo = repo
        self.projector = projector

    def process(self, cmd: Command):
        """
            Dispatch command to aggregate for processing
        :param cmd: Command
        """
        aggregate = self.repo.find(cmd.aggregate_type, cmd.aggregate_id)
        try:
            aggregate.handle_cmd(cmd)
            cmd.tracking.finished()

            # save projection of aggregate and command
            self.projector.project(cmd, aggregate=aggregate)

        except PermissionError as e:
            cmd.tracking.rejected(str(e))
            self.projector.project(cmd)

        except BaseException as e:
            cmd.tracking.failed(str(e))
            self.projector.project(cmd)
