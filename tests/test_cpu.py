from cqrs.cpu import CommandProcessor
from cqrs.aggregates import AggregateID
from cqrs.commands import Command
from unittest.mock import MagicMock, Mock
import unittest


class MockCmd(Command):
    AGGREGATE_TYPE = MagicMock


class TestCommandProcessor(unittest.TestCase):

    def test_process_command(self):
        mock_projector = MagicMock()
        mock_agg = MagicMock()
        mock_repo = MagicMock()
        mock_repo.find.return_value = mock_agg

        c = MockCmd(AggregateID.create())

        cpu = CommandProcessor(mock_repo, mock_projector)
        cpu.process(c)

        mock_agg.handle_cmd.assert_called_with(c)
        self.assertEqual(c.tracking.status, c.tracking.CommandStatus.COMPLETED)

    def test_permission_error_during_processing(self):
        msg = "TestPermissionError"

        mock_projector = MagicMock()
        mock_agg = MagicMock()
        mock_agg.handle_cmd = Mock(side_effect=PermissionError(msg))
        mock_repo = MagicMock()
        mock_repo.find.return_value = mock_agg

        c = MockCmd(AggregateID.create())

        cpu = CommandProcessor(mock_repo, mock_projector)
        cpu.process(c)

        self.assertEqual(c.tracking.status, c.tracking.CommandStatus.REJECTED)
        self.assertEqual(c.tracking.message, msg)

    def test_unexpected_error_during_processing(self):
        msg = "TestServerError"

        mock_projector = MagicMock()
        mock_agg = MagicMock()
        mock_agg.handle_cmd = Mock(side_effect=BaseException(msg))
        mock_repo = MagicMock()
        mock_repo.find.return_value = mock_agg

        c = MockCmd(AggregateID.create())

        cpu = CommandProcessor(mock_repo, mock_projector)
        cpu.process(c)

        self.assertEqual(c.tracking.status, c.tracking.CommandStatus.FAILED)
        self.assertEqual(c.tracking.message, msg)
