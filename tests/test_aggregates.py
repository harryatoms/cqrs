from cqrs.aggregates import AggregateID, Aggregate
import unittest
import pytest


class TestAggregateID(unittest.TestCase):

    def test_string_representation(self):
        test_id = '123ABC'
        agg_id = AggregateID(test_id)
        self.assertEqual(str(agg_id), test_id)


class TestAggregate(unittest.TestCase):

    def test_create_aggregate_with_id(self):
        agg_id = AggregateID.create()
        aggregate = Aggregate(agg_id)
        self.assertEqual(aggregate.id, agg_id)

    def test_aggregate_must_implement_command_handler(self):
        agg_id = AggregateID.create()
        aggregate = Aggregate(agg_id)
        with pytest.raises(NotImplementedError):
            aggregate.handle_cmd(dict())