from cqrs.commands import Command, TrackingInformation
from cqrs.aggregates import AggregateID, Aggregate
import unittest
import pytest


class TestCommand(unittest.TestCase):

    def test_command_without_payload(self):
        agg_id = AggregateID.create()
        c = Command(agg_id)

        self.assertEqual(c.aggregate_id, agg_id)
        self.assertEqual(c.payload, dict())

    def test_command_with_payload(self):
        agg_id = AggregateID.create()
        pyld = dict(one='two')
        c = Command(agg_id, pyld)

        self.assertEqual(c.aggregate_id, agg_id)
        self.assertEqual(c.payload, pyld)

    def test_command_must_indicate_aggregate_type(self):
        cmd = Command(AggregateID.create())
        with pytest.raises(ValueError):
            cmd.aggregate_type

    def test_can_retrieve_aggregate_type_from_command(self):
        class MockCmd(Command):
            AGGREGATE_TYPE = Aggregate

        c = MockCmd(AggregateID.create())
        self.assertEqual(c.aggregate_type, Aggregate)


class TestCommandTracking(unittest.TestCase):

    def test_initial_state(self):
        c = Command(AggregateID.create())
        info = c.tracking

        self.assertEqual(info.status, TrackingInformation.CommandStatus.CREATED)
        self.assertIsNotNone(info.created_at)
        self.assertIsNone(info.completed_at)
        self.assertIsNone(info.message)

    def test_completed_task_state(self):
        c = Command(AggregateID.create())
        c.tracking.finished()

        info = c.tracking
        self.assertEqual(info.status, TrackingInformation.CommandStatus.COMPLETED)
        self.assertIsNotNone(info.created_at)
        self.assertIsNotNone(info.completed_at)
        self.assertIsNone(info.message)

    def test_failed_task_state(self):
        c = Command(AggregateID.create())
        msg = 'Test-Failure'
        c.tracking.failed(msg)

        info = c.tracking
        self.assertEqual(info.status, TrackingInformation.CommandStatus.FAILED)
        self.assertIsNotNone(info.created_at)
        self.assertIsNotNone(info.completed_at)
        self.assertEqual(info.message, msg)

    def test_rejected_task_state(self):
        c = Command(AggregateID.create())
        msg = 'Test-Rejected'
        c.tracking.rejected(msg)

        info = c.tracking
        self.assertEqual(info.status, TrackingInformation.CommandStatus.REJECTED)
        self.assertIsNotNone(info.created_at)
        self.assertIsNotNone(info.completed_at)
        self.assertEqual(info.message, msg)