from cqrs.repository import AggregateRepository
from cqrs.aggregates import AggregateID, Aggregate
import unittest


class TestAggregateRepository(unittest.TestCase):

    def test_aggregate_lookup(self):
        agg_id = AggregateID.create()
        repo = AggregateRepository()
        aggregate = repo.find(Aggregate, agg_id)
        self.assertEqual(aggregate.id, agg_id)
        self.assertIsInstance(aggregate, Aggregate)
